package com.ctc.property.meshintegration;

import com.ctc.property.meshintegration.entity.MeshIntegrationRequest;
import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.entity.ProjectDetail;
import com.ctc.property.meshintegration.salesforce.Client;
import com.ctc.property.meshintegration.service.MeshIntegrationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class MeshIntegrationServiceImplTest {

    @Autowired
    private MeshIntegrationService service;

    @Value("${sqs.message_queue_url}")
    private String messageQueueUrl;

    @Value("${sqs.access_key}")
    private String accessKey;

    @Value("${sqs.access_token}")
    private String accessToken;

    @Value("${sqs.region}")
    private String region;

    @Test
    public void testReceiveMeshIntegrationMessageFromQueue() {
        String message = "{ \"orgId\":\"00D7F000003CcNwUAK\", \"projectId\":\"5630be58204b89c90b2034f0\", \"key\":\"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\", \"pass\":\"ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\", \"url\":\"https://propertyreports.mesh-staging.com\", \"fromDateTime\":\"2019-02-07 04:05:09 \", \"toDateTime\":\"2019-03-07 21:12:57 \" }";
        for (int i=0; i<30;i++)
            sendMessageToQueue(message);

        List<MeshIntegrationRequest> meshIntegrationRequests = service.receiveMeshIntegrationRequestFromQueue();
        assertEquals(30,meshIntegrationRequests.size());
        /*
        assertEquals("00D7F000003CcNwUAK",meshIntegrationRequests.get(0).getOrgId());
        assertEquals("5630be58204b89c90b2034f0",meshIntegrationRequests.get(0).getProjectId());
        assertEquals("https://propertyreports.mesh-staging.com",meshIntegrationRequests.get(0).getUrl());
        assertEquals("IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w",meshIntegrationRequests.get(0).getKey());
        assertEquals("ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",meshIntegrationRequests.get(0).getPass());
        assertEquals("2019-02-07 04:05:09 ",meshIntegrationRequests.get(0).getFromDateTime());
        assertEquals("2019-03-07 21:12:57 ",meshIntegrationRequests.get(0).getToDateTime());*/
    }

    @Test
    public void testNotReceiveErrorMessageFromQueue() {
        final String string = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjVGQkU4QTA2RTQ4NjBBMUQxMENFNDYzN0JFRENCQTMyQTI4NjFFNkYiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJYNzZLQnVTR0NoMFF6a1kzdnR5Nk1xS0dIbTgifQ.eyJuYmYiOjE1NTIyNjA2NzYsImV4cCI6MTU1MjMwMzg3NiwiaXNzIjoiaHR0cHM6Ly9jbGllbnQubWVzaC1zdGFnaW5nLmNvbSIsImF1ZCI6WyJodHRwczovL2NsaWVudC5tZXNoLXN0YWdpbmcuY29tL3Jlc291cmNlcyIsIm1lc2gucHJvcGVydHkucmVwb3J0cyJdLCJjbGllbnRfaWQiOiJEODdDREJFQy00NjJFLUU5MTEtQjhCMy0wMDBEM0FFMDIyOTQiLCJjbGllbnRfY2xpZW50X2lkIjoiRDg3Q0RCRUMtNDYyRS1FOTExLUI4QjMtMDAwRDNBRTAyMjk0Iiwic2NvcGUiOlsibWVzaC5wcm9wZXJ0eS5yZXBvcnRzIl19.k0kaSN-G8IiT01TvEM6z8HdcK8EYzFRL8q0H6RHYCOFSLCj4_0urIkLg-7F-YjgexszE6E8Nr6QpGaQ5Yd2q1-ddn0zdsle4SgfuRDWL4-pW4cntCjONXae-qMqoKcAIPLO-Q5etyHVkFv2BxZOGeaV6Q02OsbQIG5RkmGl2ZkrtmYOSfsA3iu7uYPOZVBHfggqqDKpH6Xje1zFNioy_-Bz3mupWDZaX6mZQpskVwCb4u3-HBHjwLrL7krSGF83vFjWZBv_fzNhlxt28ZTA2VkWU3bC3PGr7CS-4xqwhL6fse-zTZ0wv27yufFowBBMu6vKJeJ-9OVybf1hMMDx-Ow";
        String messageWithoutOrgId = "{ \"projectId\":\"5630be58204b89c90b2034f0\", \"key\":\"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\", \"pass\":\"ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\", \"url\":\"https://propertyreports.mesh-staging.com\", \"fromDateTime\":\"2019-02-07 04:05:09 \", \"toDateTime\":\"2019-03-07 21:12:57 \" }";
        for (int i=0; i<30;i++)
            sendMessageToQueue(messageWithoutOrgId);
        List<MeshIntegrationRequest> meshIntegrationRequests = service.receiveMeshIntegrationRequestFromQueue();
        assertEquals(0,meshIntegrationRequests.size());
    }

    @Test
    public void test1(){

        List<MeshIntegrationRequest> meshIntegrationRequestList = new ArrayList<>();
        MeshIntegrationRequest meshIntegrationRequest = new MeshIntegrationRequest();


        Date date = new Date();
        date.setTime(1525462800000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println(sdf.format(date));

        meshIntegrationRequest.setFromDateTime(date.getTime());
        meshIntegrationRequest.setOrgId("00D7F000003CcNwUAK");
        ProjectDetail projectDetail1 = new ProjectDetail();

        /*


     "url":"https://propertyreports.mesh-staging.com",
     "projectId":"5630be58204b89c90b2034f0",
    "pass":"ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
    "key":"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w"
         */
        projectDetail1.setPass("ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=");
                                //ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=
        projectDetail1.setKey("IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w");
                              //"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w"
        projectDetail1.setProjectId("5630be58204b89c90b2034f0");
        projectDetail1.setUrl("https://propertyreports.mesh-staging.com");
                               //https://propertyreports.mesh-staging.com
        meshIntegrationRequest.getProjectDetails().add(projectDetail1);
        meshIntegrationRequestList.add(meshIntegrationRequest);


        Map<OrganisationSalesforceInformation, List<Client>> organisationClientMap =service.retrieveClientsFromMeshAPI(meshIntegrationRequestList);

        //Map<String, List<Client>> map =  service.receiveClientsInOrganisation(meshIntegrationRequestList);
        System.out.println();
        //meshDataWithOrgIdList =  service.receiveMeshData(meshIntegrationRequestList);
        System.out.println();
    }

    @Test
    public void test2(){

        String message = "{ \"orgId\":\"00DO000000531JPMAY\", \"projectId\":\"5630be58204b89c90b2034f0\", \"key\":\"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\", \"pass\":\"ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\", \"url\":\"https://propertyreports.mesh-staging.com\", \"fromDateTime\":\"2019-02-07 04:05:09 \", \"toDateTime\":\"2019-03-07 21:12:57 \" }";
        sendMessageToQueue(message);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        service.process();
    }

    @Test
    public void test4(){
        Map<OrganisationSalesforceInformation, List<Client>> map = new HashMap<>();
        List<Client> clients = new ArrayList<>();
        /*
        Client client1 = new Client();
        client1.setFirstName("Lionel");
        client1.setLastName("Messi");
        client1.setEmail("messi@gmail.com");
        client1.setLandline("0360907766");
        client1.setMobile("0429008390");
        client1.setTranscriptNo("transcriptId1");
        client1.setInformation("info1");

        Client client2 = new Client();
        client2.setFirstName("Micheal");
        client2.setLastName("Owen");
        client2.setEmail("owen@gmail.com");
        client2.setLandline("0245678962");
        client2.setTranscriptNo("transcriptId2");

        clients.add(client2);
        clients.add(client1);

        Client client3 = new Client();
        client3.setFirstName("dingling");
        client3.setLastName("wu");
        client3.setEmail("wu@gmail.com");
        client3.setLandline("0267824109");
        client3.setTranscriptNo("transcriptId3");

        clients.add(client3);

        Client client4 = new Client();
        client4.setFirstName("beixin");
        client4.setLastName("zhang");
        client4.setEmail("zhangbeixin@gmail.com");
        client4.setLandline("0397824651");
        client4.setTranscriptNo("transcriptId4");
        client4.setInformation("info4");


        clients.add(client4);


        Client client5 = new Client();
        client5.setFirstName("hongxing");
        client5.setLastName("tu");
        client5.setEmail("tu9087@gmail.com");
        client5.setLandline("0278407003");
        client5.setTranscriptNo("transcriptId5");

        clients.add(client5);



        Client client6 = new Client();
        client6.setFirstName("kong");
        client6.setLastName("shitu");
        client6.setEmail("shitu891029@gmail.com");
        client6.setLandline("0280700372");
        client6.setTranscriptNo("transcriptId6");
        clients.add(client6);


        Client client7 = new Client();
        client7.setFirstName("dahong");
        client7.setLastName("shi");
        client7.setEmail("shi726@gmail.com");
        client7.setLandline("0390560204");
        client7.setTranscriptNo("transcriptId7");
        client7.setInformation("info7");
        clients.add(client7);*/

        Client client8 = new Client();
        client8.setFirstName("zheting");
        client8.setLastName("liang");
        client8.setEmail("liangtl87@gmail.com");
        client8.setLandline("0760566788");
        client8.setTranscriptNo("transcriptId8");
        client8.setInformation("info8....");
        clients.add(client8);

        Client client9 = new Client();
        client9.setFirstName("Zhedong");
        client9.setLastName("Fan");
        client9.setEmail("fan97@gmail.com");
        client9.setLandline("0369074381");
        client9.setTranscriptNo("transcriptId9");
        client9.setInformation("info9....");
        clients.add(client9);


        Client client10 = new Client();
        client10.setFirstName("Long");
        client10.setLastName("Ma");
        client10.setLandline("01869074381");
        client10.setTranscriptNo("transcriptId10");
        client10.setInformation("info10..fdadsdfdsafdsafasdfd..\nasfdassadfads\nasfdsafasdfdsafdsa\nsfdasdfadsfadsfadsfadsfdas2342dsfvasdfasdsadfdskllk;vnvkndf    fsf\n\n");
        clients.add(client10);

        OrganisationSalesforceInformation organisationSalesforceInformation = new OrganisationSalesforceInformation("00DO000000531JPMAY", "");

        map.put(organisationSalesforceInformation,clients);
        service.pushInformationToSalesforce(map);
    }




    private void sendMessageToQueue(String message){
        //final SQSMessageHandler sqsMessageHandler = new SQSMessageHandler(messageQueueUrl, accessKey, accessToken, region);
        //sqsMessageHandler.sendMessage(message);
    }

}
