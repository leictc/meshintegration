package com.ctc.property.meshintegration;

import com.amazonaws.services.sqs.model.Message;
import com.ctc.property.meshintegration.Util.MeshIntegrationUtil;
import com.ctc.property.meshintegration.entity.MeshIntegrationRequest;
import com.ctc.property.meshintegration.entity.MeshToken;
import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.entity.ProjectDetail;
import com.ctc.property.meshintegration.salesforce.Client;
import com.ctc.property.meshintegration.service.MeshAPIService;
import com.ctc.property.meshintegration.service.MeshIntegrationService;
import com.ctc.property.meshintegration.service.MeshIntegrationServiceImpl;
import com.ctc.property.meshintegration.service.MessageService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
class MeshIntegrationTest {

    @Mock
    private MessageService messageService;

    @Mock
    private MeshAPIService meshAPIService;

    @InjectMocks
    private MeshIntegrationService meshIntegrationService = new MeshIntegrationServiceImpl();

    /*@BeforeEach
    void setMockOutput() {
        Message message = new Message();
        String jsonBody = "{ \"orgId\":\"00D7F444403CcNwUAK\", \"projectId\":\"5630be77774b89c90b2034f0\", \"key\":\"IFz2ppnQ3QXEPmzYf555558lswysCfWqCML6v6XSi5w\", \"pass\":\"ujGPiMkTFMW3yDU+wIMbfIOG8x704uhi829cOoOI6AqJInYophuczble/HriUdT5uyL7RyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\", \"url\":\"https://propertyreports.mesh-staging.com\", \"fromDateTime\":\"2019-02-07 04:05:09 \", \"toDateTime\":\"2019-03-07 21:12:57 \" }";
        message.setBody(jsonBody);
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(messageService.receiveMessage()).thenReturn(messageList);
    }*/

    @DisplayName("Test Receiving Mesh Integration Message From Queue")
    @Test
    void testReceiveMeshIntegrationMessageFromQueue() {

        Message message = new Message();
        String jsonBody = "{ \"orgId\":\"00D7F444403CcNwUAK\", \"projectId\":\"5630be77774b89c90b2034f0\"," +
                " \"key\":\"IFz2ppnQ3QXEPmzYf555558lswysCfWqCML6v6XSi5w\", " +
                "\"pass\":\"ujGPiMkTFMW3yDU+wIMbfIOG8x704uhi829cOoOI6AqJInYophuczble/HriUdT5uyL7RyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\", " +
                "\"url\":\"https://propertyreports.mesh-staging.com\"," +
                " \"fromDateTime\":\"2019-02-07 04:05:09 \"," +
                " \"toDateTime\":\"2019-03-07 21:12:57 \" }";
        message.setBody(jsonBody);
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(messageService.receiveMessage()).thenReturn(messageList);

        List<MeshIntegrationRequest> meshIntegrationRequests = meshIntegrationService.receiveMeshIntegrationRequestFromQueue();
        /*
        assertEquals("00D7F444403CcNwUAK",meshIntegrationRequests.get(0).getOrgId());
        assertEquals("5630be77774b89c90b2034f0",meshIntegrationRequests.get(0).getProjectId());
        assertEquals("https://propertyreports.mesh-staging.com",meshIntegrationRequests.get(0).getUrl());
        assertEquals("IFz2ppnQ3QXEPmzYf555558lswysCfWqCML6v6XSi5w",meshIntegrationRequests.get(0).getKey());
        assertEquals("ujGPiMkTFMW3yDU+wIMbfIOG8x704uhi829cOoOI6AqJInYophuczble/HriUdT5uyL7RyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",meshIntegrationRequests.get(0).getPass());*/
        assertEquals("2019-02-07 04:05:09 ",meshIntegrationRequests.get(0).getFromDateTime());
        assertEquals("2019-03-07 21:12:57 ",meshIntegrationRequests.get(0).getToDateTime());
    }

    private static final String URL = "https://propertyreports.mesh-staging.com";
    private static final String PASS = "ujGPiMkTFMW3yDU+wIMbfIOG8x704uhi829cOoOI6AqJInYophuczble/HriUdT5uyL7RyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=";
    private static final String KEY = "IFz2ppnQ3QXEPmzYf555558lswysCfWqCML6v6XSi5w";
    private static final String ORG_ID = "00D7F444403CcNwUAK";
    private static final String PROJECT_ID = "5630be77774b89c90b2034f0";
    private static final long FROM_DATE_TIME = 1555459200000L;

    private static final String transcriptData1 = "{\n  \"response\": {\n    \"meta\": {\n      \"totalRecords\": 5\n    },\n    \"status\": [\n      {\n        \"code\": \"200\",\n      " +
            "  \"type\": \"OK\",\n        \"message\": \"The request succeeded\"\n      }\n    ],\n    \"data\": [\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Email Address\",\n        \"dataType\": \"Email\",\n      " +
            "  \"dataValue\": \"\",\n        \"creationDate\": \"2019-02-25T02:55:05.227\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Email Address\",\n     " +
            "   \"dataType\": \"Email\",\n        \"dataValue\": \"test@meshtesting.com\",\n        \"creationDate\": \"2019-02-17T23:20:12.887\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Name\",\n       " +
            " \"dataType\": \"Name\",\n        \"dataValue\": \u201Ctest22\",\n        \"creationDate\": \"2019-02-25T02:55:05.227\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Name\",\n        \"dataType\": \"Name\",\n   " +
            "     \"dataValue\": \"test\",\n        \"creationDate\": \"2019-02-17T23:20:12.887\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Phone\",\n        \"dataType\": \"Text\",\n        \"dataValue\": \"\",\n     " +
            "   \"creationDate\": \"2019-02-17T23:20:12.887\"\n      }\n    ]\n  }\n}";

    private static final String transcriptData2 = "{\n  \"response\": {\n    \"meta\": {\n      \"totalRecords\": 5\n    },\n    \"status\": [\n      {\n        \"code\": \"200\",\n        \"type\": \"OK\",\n        \"message\": \"The request succeeded\"\n      }\n    ],\n    \"data\": [\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Email Address\",\n        \"dataType\": \"Email\",\n        \"dataValue\": \"\",\n        \"creationDate\": \"2019-02-25T02:55:05.227\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Email Address\",\n        \"dataType\": \"Email\",\n        \"dataValue\": \"test@meshtesting.com\",\n        \"creationDate\": \"2019-02-17T23:20:12.887\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Name\",\n        \"dataType\": \"Name\",\n        \"dataValue\": \u201CSteve\",\n        \"creationDate\": \"2019-02-25T02:55:05.227\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Name\",\n        \"dataType\": \"Name\",\n        \"dataValue\": \"test\",\n        \"creationDate\": \"2019-02-17T23:20:12.887\"\n      },\n      {\n        \"transcriptNo\": \"TNSFED7FC260\",\n        \"dataName\": \"Phone\",\n        \"dataType\": \"Text\",\n        \"dataValue\": \"0477888123\",\n        \"creationDate\": \"2019-02-17T23:20:12.887\"\n      }\n    ]\n  }\n}\n";



    private static final String meshJsonData = "{\n" +
            "  \"response\": {\n" +
            "    \"meta\": {\n" +
            "      \"totalRecords\": 42\n" +
            "    },\n" +
            "    \"status\": [\n" +
            "      {\n" +
            "        \"code\": \"200\",\n" +
            "        \"type\": \"OK\",\n" +
            "        \"message\": \"The request succeeded\"\n" +
            "      }\n" +
            "    ],\n" +
            "    \"data\": [\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS8306BD043\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-04-10T05:26:35.283\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS8306BD043\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-04-10T05:26:35.283\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS8306BD043\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-04-10T05:26:35.283\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"lei@propic.com.au\",\n" +
            "        \"creationDate\": \"2019-04-10T05:32:16.903\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"Lei\",\n" +
            "        \"creationDate\": \"2019-04-10T05:32:16.903\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"12345678\",\n" +
            "        \"creationDate\": \"2019-04-10T05:32:16.903\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Additional Information\",\n" +
            "        \"dataType\": \"TextMultiline\",\n" +
            "        \"dataValue\": \"additional information\",\n" +
            "        \"creationDate\": \"2019-04-10T05:33:45.267\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"bill@ms.com\",\n" +
            "        \"creationDate\": \"2019-04-10T05:39:51.88\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"Bill\",\n" +
            "        \"creationDate\": \"2019-04-10T05:39:51.88\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"90908800\",\n" +
            "        \"creationDate\": \"2019-04-10T05:39:51.88\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Additional Information\",\n" +
            "        \"dataType\": \"TextMultiline\",\n" +
            "        \"dataValue\": \"876\",\n" +
            "        \"creationDate\": \"2019-04-10T05:40:26.917\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSF045397A4\",\n" +
            "        \"dataName\": \"Address\",\n" +
            "        \"dataType\": \"Address\",\n" +
            "        \"dataValue\": \"40 Glasshouse Rd, Collingwood VIC 3066, Australia\",\n" +
            "        \"creationDate\": \"2019-04-10T06:20:37.687\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSF045397A4\",\n" +
            "        \"dataName\": \"Date of Birth\",\n" +
            "        \"dataType\": \"DateTime\",\n" +
            "        \"dataValue\": \"10 April 2019 4:25 PM\",\n" +
            "        \"creationDate\": \"2019-04-10T06:20:37.687\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSF045397A4\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"daire@gmail.com\",\n" +
            "        \"creationDate\": \"2019-04-10T06:20:37.687\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSF045397A4\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"Daire test\",\n" +
            "        \"creationDate\": \"2019-04-10T06:20:37.687\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSF045397A4\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"3423443\",\n" +
            "        \"creationDate\": \"2019-04-10T06:20:37.687\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSF045397A4\",\n" +
            "        \"dataName\": \"Phone (Mobile)\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"3434243\",\n" +
            "        \"creationDate\": \"2019-04-10T06:20:37.687\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSFED7FC260\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-25T02:55:05.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSFED7FC260\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"test@meshtesting.com\",\n" +
            "        \"creationDate\": \"2019-02-17T23:20:12.887\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSFED7FC260\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-25T02:55:05.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSFED7FC260\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"test\",\n" +
            "        \"creationDate\": \"2019-02-17T23:20:12.887\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSFED7FC260\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-17T23:20:12.887\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS962D6EAC9\",\n" +
            "        \"dataName\": \"Additional Information\",\n" +
            "        \"dataType\": \"TextMultiline\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-21T03:56:55.453\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS3B1C870C0\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"fatek@propic.com.au\",\n" +
            "        \"creationDate\": \"2019-02-24T21:05:08.547\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS3B1C870C0\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"Fatek\",\n" +
            "        \"creationDate\": \"2019-02-24T21:05:08.547\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS3B1C870C0\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"0466383930\",\n" +
            "        \"creationDate\": \"2019-02-24T21:05:08.547\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS3B1C870C0\",\n" +
            "        \"dataName\": \"Additional Information\",\n" +
            "        \"dataType\": \"TextMultiline\",\n" +
            "        \"dataValue\": \"Test\",\n" +
            "        \"creationDate\": \"2019-02-24T21:05:44.91\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSDFD36BAB8\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-25T02:45:32.077\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSDFD36BAB8\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-25T02:45:32.077\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNSDFD36BAB8\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"\",\n" +
            "        \"creationDate\": \"2019-02-25T02:45:32.077\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Address\",\n" +
            "        \"dataType\": \"Address\",\n" +
            "        \"dataValue\": null,\n" +
            "        \"creationDate\": \"2019-04-11T21:51:39.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Date of Birth\",\n" +
            "        \"dataType\": \"DateTime\",\n" +
            "        \"dataValue\": \"01 April 2019 7:05 AM\",\n" +
            "        \"creationDate\": \"2019-04-11T21:51:39.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": null,\n" +
            "        \"creationDate\": \"2019-04-11T21:51:39.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"lei\",\n" +
            "        \"creationDate\": \"2019-04-11T21:51:39.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": null,\n" +
            "        \"creationDate\": \"2019-04-11T21:51:39.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS56763A378\",\n" +
            "        \"dataName\": \"Phone (Mobile)\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"0455666123\",\n" +
            "        \"creationDate\": \"2019-04-11T21:51:39.227\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Address\",\n" +
            "        \"dataType\": \"Address\",\n" +
            "        \"dataValue\": \"2 George St, Sydney NSW 2000, Australia\",\n" +
            "        \"creationDate\": \"2019-04-11T22:45:39.697\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Date of Birth\",\n" +
            "        \"dataType\": \"DateTime\",\n" +
            "        \"dataValue\": \"28 November 2018 8:45 AM\",\n" +
            "        \"creationDate\": \"2019-04-11T22:45:39.697\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Email Address\",\n" +
            "        \"dataType\": \"Email\",\n" +
            "        \"dataValue\": \"steve@apple.com\",\n" +
            "        \"creationDate\": \"2019-04-11T22:45:39.697\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Name\",\n" +
            "        \"dataType\": \"Name\",\n" +
            "        \"dataValue\": \"steve\",\n" +
            "        \"creationDate\": \"2019-04-11T22:45:39.697\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Phone\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"0280774443\",\n" +
            "        \"creationDate\": \"2019-04-11T22:45:39.697\"\n" +
            "      },\n" +
            "      {\n" +
            "        \"transcriptNo\": \"TNS5036AC2CC\",\n" +
            "        \"dataName\": \"Phone (Mobile)\",\n" +
            "        \"dataType\": \"Text\",\n" +
            "        \"dataValue\": \"0411222345\",\n" +
            "        \"creationDate\": \"2019-04-11T22:45:39.697\"\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "}";

    @DisplayName("Test Receiving Client Information From Mesh API")
    @Test
    void testReceiveClientsInformationFromMeshAPI(){

        MeshIntegrationRequest meshIntegrationRequest = new MeshIntegrationRequest();
        meshIntegrationRequest.setOrgId(ORG_ID);
        meshIntegrationRequest.setFromDateTime(FROM_DATE_TIME);

        ProjectDetail projectDetail = new ProjectDetail();
        projectDetail.setUrl(URL);
        projectDetail.setPass(PASS);
        projectDetail.setProjectId(PROJECT_ID);
        projectDetail.setKey(KEY);
        meshIntegrationRequest.getProjectDetails().add(projectDetail);
        List<MeshIntegrationRequest> meshIntegrationRequestList = new ArrayList<>();
        meshIntegrationRequestList.add(meshIntegrationRequest);

        MeshToken meshToken = new MeshToken("token", LocalDateTime.now());
        when(meshAPIService.retrieveToken(any(),any())).thenReturn(Optional.of(meshToken));
        when(meshAPIService.retrieveData(any(),any())).thenReturn(Optional.of(meshJsonData));
        //when(meshAPIService.retrieveTranscripts(any(),any())).thenReturn(Optional.of(jsonData));
        meshIntegrationService.retrieveClientsFromMeshAPI(meshIntegrationRequestList);
    }

    @DisplayName("Test Receiving Client Information From Mesh API")
    @Test
    void testSearchClientsInSalesforce(){
        Map<String, List<Client>> map = new HashMap<>();
        List<Client> clients = new ArrayList<>();
        Client client1 = new Client();
        client1.setFirstName("Henry");
        client1.setLastName("Woods");
        client1.setEmail("woods@gmail.com.com");
        client1.setLandline("0212345678");
        client1.setMobile("041244333123");
        Client client2 = new Client();
        client2.setFirstName("Dean");
        client2.setLastName("Lee");
        client2.setEmail("lee@gmail.com");
        client2.setLandline("0388345678");
        client2.setMobile("048099933223");
        clients.add(client1);
        clients.add(client2);

        map.put("00DO000000531JPMAY",clients);
        //meshIntegrationService.pushInformationToSalesforce(map);

    }

    @DisplayName("Test Converting Mesh Integration Request ")
    @Test
    public void testConvertingJsonMeshIntegrationRequest() {
        //Converting a invalid request message
        String json = "safasdfsfsda2343.';'[\nsadfsadf234,;`\"";
        Optional<MeshIntegrationRequest>  meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);
        assertFalse(meshIntegrationRequest.isPresent());

        //Converting a eligible request message
        json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports1.mesh-staging.com/\",\n    \"projectId\":\"5730be58204b89c90b2034f0\",\n    \"pass\":\"ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"TEST5630be58204b89c90b2034f0\",\n    \"pass\":\"TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"00DO000000531JPMAY\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":1555459200000\n}";
        meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);

        assertTrue(meshIntegrationRequest.isPresent());
        assertEquals(1555459200000L, meshIntegrationRequest.get().getFromDateTime());
        assertEquals("00DO000000531JPMAY", meshIntegrationRequest.get().getOrgId());
        assertEquals("prpt_", meshIntegrationRequest.get().getNamespace());

        assertEquals(2, meshIntegrationRequest.get().getProjectDetails().size());
        assertEquals("5730be58204b89c90b2034f0", meshIntegrationRequest.get().getProjectDetails().get(0).getProjectId());
        assertEquals("https://propertyreports1.mesh-staging.com/", meshIntegrationRequest.get().getProjectDetails().get(0).getUrl());
        assertEquals("ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.get().getProjectDetails().get(0).getPass());
        assertEquals("IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.get().getProjectDetails().get(0).getKey());

        assertEquals("TEST5630be58204b89c90b2034f0", meshIntegrationRequest.get().getProjectDetails().get(1).getProjectId());
        assertEquals("https://test.propertyreports.mesh-staging.com/", meshIntegrationRequest.get().getProjectDetails().get(1).getUrl());
        assertEquals("TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.get().getProjectDetails().get(1).getPass());
        assertEquals("TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.get().getProjectDetails().get(1).getKey());

        //Converting a request message without organisation id
        json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports1.mesh-staging.com/\",\n    \"projectId\":\"5730be58204b89c90b2034f0\",\n    \"pass\":\"ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"TEST5630be58204b89c90b2034f0\",\n    \"pass\":\"TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":1555459200000\n}";
        meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);
        assertFalse(meshIntegrationRequest.isPresent());


        //Converting a request message without from datetime
        json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports1.mesh-staging.com/\",\n    \"projectId\":\"5730be58204b89c90b2034f0\",\n    \"pass\":\"ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"TEST5630be58204b89c90b2034f0\",\n    \"pass\":\"TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"00DO000000531JPMAY\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":null\n}";
        meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);
        assertFalse(meshIntegrationRequest.isPresent());

        //Converting a request message without any project id
        json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports1.mesh-staging.com/\",\n    \"projectId\":\"\",\n    \"pass\":\"ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"\",\n    \"pass\":\"TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":1555459200000\n}";
        meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);
        assertFalse(meshIntegrationRequest.isPresent());

        //Converting a request message with one of project detail containing no pass
        json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports1.mesh-staging.com/\",\n    \"projectId\":\"5730be58204b89c90b2034f0\",\n    \"pass\":\"ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"TEST5630be58204b89c90b2034f0\",\n    \"pass\":\"\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"00DO000000531JPMAY\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":1555459200000\n}";
        meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);

        assertTrue(meshIntegrationRequest.isPresent());
        assertEquals(1555459200000L, meshIntegrationRequest.get().getFromDateTime());
        assertEquals("00DO000000531JPMAY", meshIntegrationRequest.get().getOrgId());
        assertEquals("prpt_", meshIntegrationRequest.get().getNamespace());

        assertEquals(1, meshIntegrationRequest.get().getProjectDetails().size());
        assertEquals("5730be58204b89c90b2034f0", meshIntegrationRequest.get().getProjectDetails().get(0).getProjectId());
        assertEquals("https://propertyreports1.mesh-staging.com/", meshIntegrationRequest.get().getProjectDetails().get(0).getUrl());
        assertEquals("ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.get().getProjectDetails().get(0).getPass());
        assertEquals("IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.get().getProjectDetails().get(0).getKey());

        //Converting a eligible request message with from date time as text
        json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports1.mesh-staging.com/\",\n    \"projectId\":\"5730be58204b89c90b2034f0\",\n    \"pass\":\"ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"TEST5630be58204b89c90b2034f0\",\n    \"pass\":\"TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"00DO000000531JPMAY\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":\"1555459200000\"\n}";
        meshIntegrationRequest = MeshIntegrationUtil.toMeshIntegrationRequest(json);

        assertEquals(1555459200000L, meshIntegrationRequest.get().getFromDateTime());
        assertEquals("00DO000000531JPMAY", meshIntegrationRequest.get().getOrgId());
        assertEquals("prpt_", meshIntegrationRequest.get().getNamespace());

        assertEquals(2, meshIntegrationRequest.get().getProjectDetails().size());
        assertEquals("5730be58204b89c90b2034f0", meshIntegrationRequest.get().getProjectDetails().get(0).getProjectId());
        assertEquals("https://propertyreports1.mesh-staging.com/", meshIntegrationRequest.get().getProjectDetails().get(0).getUrl());
        assertEquals("ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.get().getProjectDetails().get(0).getPass());
        assertEquals("IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.get().getProjectDetails().get(0).getKey());

        assertEquals("TEST5630be58204b89c90b2034f0", meshIntegrationRequest.get().getProjectDetails().get(1).getProjectId());
        assertEquals("https://test.propertyreports.mesh-staging.com/", meshIntegrationRequest.get().getProjectDetails().get(1).getUrl());
        assertEquals("TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.get().getProjectDetails().get(1).getPass());
        assertEquals("TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.get().getProjectDetails().get(1).getKey());

    }


    @DisplayName("Test Receiving Message From Message Queue")
    @Test
    void testReceivingMessageFromMessageQueue(){
        String json = "{\n    \"toDateTime\":1555462800000,\n    \"projectDetails\":[\n    {\n        \"url\":\"https://propertyreports.mesh-staging.com/\",\n    \"projectId\":\"5630be58204b89c90b2034f0\",\n    \"pass\":\"ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n\n    {\"url\":\"https://test.propertyreports.mesh-staging.com/\",\"projectId\":\"TEST5630be58204b89c90b2034f0\",\n    \"pass\":\"TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"},\n\n    {\"url\":\"https://propertyreports.mesh-staging.com\",\"projectId\":\"5630be58204b89c90b2034f0\",\n    \"pass\":\"ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=\",\"key\":\"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w\"}\n\n    ],\n    \"orgId\":\"00DO000000531JPMAY\",\n    \"namespace\":\"prpt_\",\n    \"fromDateTime\":1555459200000\n}";
        Message message = new Message();
        message.setBody(json);
        List<Message> messageList = new ArrayList<>();
        messageList.add(message);
        when(messageService.receiveMessage()).thenReturn(messageList);
        List<MeshIntegrationRequest> meshIntegrationRequestList = meshIntegrationService.receiveMeshIntegrationRequestFromQueue();
        assertEquals(1, meshIntegrationRequestList.size());

        MeshIntegrationRequest meshIntegrationRequest = meshIntegrationRequestList.get(0);
        assertEquals(1555459200000L, meshIntegrationRequest.getFromDateTime());
        assertEquals("00DO000000531JPMAY", meshIntegrationRequest.getOrgId());
        assertEquals("prpt_", meshIntegrationRequest.getNamespace());

        assertEquals(3, meshIntegrationRequest.getProjectDetails().size());
        /*
        assertEquals("5730be58204b89c90b2034f0", meshIntegrationRequest.getProjectDetails().get(0).getProjectId());
        assertEquals("https://propertyreports1.mesh-staging.com/", meshIntegrationRequest.getProjectDetails().get(0).getUrl());
        assertEquals("ujGBBMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.getProjectDetails().get(0).getPass());
        assertEquals("IFz2ccnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.getProjectDetails().get(0).getKey());

        assertEquals("TEST5630be58204b89c90b2034f0", meshIntegrationRequest.getProjectDetails().get(1).getProjectId());
        assertEquals("https://test.propertyreports.mesh-staging.com/", meshIntegrationRequest.getProjectDetails().get(1).getUrl());
        assertEquals("TESTujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=",
                meshIntegrationRequest.getProjectDetails().get(1).getPass());
        assertEquals("TESTIFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w", meshIntegrationRequest.getProjectDetails().get(1).getKey());*/

        //Map<String, List<Client>> organisationClientMap =meshIntegrationService.receiveClientsInOrganisation(meshIntegrationRequestList);
        System.out.println();

    }

    @DisplayName("Test Receiving Information From Mesh")
    @Test
    public void testReceivingInfoFromMesh(){

        List<MeshIntegrationRequest> meshIntegrationRequestList = new ArrayList<>();
        MeshIntegrationRequest meshIntegrationRequest = new MeshIntegrationRequest();


        Date date = new Date();
        date.setTime(1525462800000L);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.out.println(sdf.format(date));

        meshIntegrationRequest.setFromDateTime(date.getTime());
        meshIntegrationRequest.setOrgId("00D7F000003CcNwUAK");
        ProjectDetail projectDetail1 = new ProjectDetail();
        projectDetail1.setPass("ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=");
        //ujGPiMkTFJM3yDU+wIMbfIOG8x944uhi829cOoOI6EuJInYophuczble/HriUdT5uyL7LyzZMhiuNSkkDBeA7bQJyDu4YPJZ0PwF5TxhcOI=
        projectDetail1.setKey("IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w");
        //"IFz2ppnQ3QXEPmzYfWZGYV8lswysCfWqCML6v6XSi5w"
        projectDetail1.setProjectId("5630be58204b89c90b2034f0");
        projectDetail1.setUrl("https://propertyreports.mesh-staging.com");
        //https://propertyreports.mesh-staging.com
        meshIntegrationRequest.getProjectDetails().add(projectDetail1);
        meshIntegrationRequestList.add(meshIntegrationRequest);
        when(meshAPIService.retrieveToken(any(),any())).thenReturn(Optional.of(new MeshToken("token", LocalDateTime.now().plusHours(12))));
        when(meshAPIService.retrieveData(any(),any())).thenReturn(Optional.of(meshJsonData));
        List<String> transcriptList = new ArrayList<>();
        transcriptList.add(transcriptData1);
        transcriptList.add(transcriptData2);
        when(meshAPIService.retrieveTranscripts(any(),any())).thenReturn(transcriptList);

        Map<OrganisationSalesforceInformation, List<Client>> organisationClientMap =meshIntegrationService.retrieveClientsFromMeshAPI(meshIntegrationRequestList);
        OrganisationSalesforceInformation organisationSalesforceInformation = new OrganisationSalesforceInformation("00D7F000003CcNwUAK", "");
        assertEquals(2, organisationClientMap.get(organisationSalesforceInformation).size());

        //Map<String, List<Client>> map =  service.receiveClientsInOrganisation(meshIntegrationRequestList);
        System.out.println();
        //meshDataWithOrgIdList =  service.receiveMeshData(meshIntegrationRequestList);
        System.out.println();
    }


    @DisplayName("Test Receiving Information From Mesh")
    @Test
    public void testPushingInformaitonToSalesforce(){
        Map<OrganisationSalesforceInformation, List<Client>> clientOrganisationMap = new HashMap<>();
        meshIntegrationService.pushInformationToSalesforce(clientOrganisationMap);


    }

}
