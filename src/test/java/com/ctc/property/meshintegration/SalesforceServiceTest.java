package com.ctc.property.meshintegration;

import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.salesforce.Client;
import com.ctc.property.meshintegration.service.SalesforceService;
import com.ctc.property.meshintegration.service.SalesforceServiceImpl;
import com.ctc.property.meshintegration.service.SalesforceSoapService;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SalesforceServiceTest {

    @Mock
    private SalesforceSoapService salesforceSoapService;

    @InjectMocks
    private SalesforceService salesforceService = new SalesforceServiceImpl();

    @DisplayName("Test Creating One New Client And One New Client Management In Salesforce")
    @Test
    void testCreatingSingleNewClientAndSingleNewClientManagement() throws ConnectionException {
        final String clientId= "a1wO0000002Z48iIAC";
        final String clientManagementId= "a1wO0000222Z48iIAC";
        OrganisationSalesforceInformation organisationSalesforceInformation = new OrganisationSalesforceInformation("fdsa", "");
        List<Client > clients = new ArrayList<>();
        Client newClient1 = new Client();
        newClient1.setFirstName("Lionel");
        newClient1.setLastName("Messi");
        newClient1.setEmail("messi@gmail.com");
        newClient1.setLandline("0360907766");
        newClient1.setMobile("0429008390");
        newClient1.setTranscriptNo("transcriptId1");
        newClient1.setInformation("info1");
        clients.add(newClient1);

        QueryResult clientRecordTypeResult = new QueryResult();
        clientRecordTypeResult.setDone(true);
        clientRecordTypeResult.setSize(1);
        SObject sObject = new SObject();
        sObject.setId("01228000000LLXQAA4");
        SObject[] sObjects = new SObject[1];
        sObjects[0] = sObject;
        clientRecordTypeResult.setRecords(sObjects);

        QueryResult noResult = new QueryResult();
        noResult.setDone(true);
        noResult.setSize(0);
        when(salesforceSoapService.query(any())).thenReturn(clientRecordTypeResult).thenReturn(noResult);
        SaveResult[] clientSaveResults = new SaveResult[1];
        SaveResult saveClientResult = new SaveResult();
        saveClientResult.setId(clientId);
        saveClientResult.setSuccess(true);
        clientSaveResults[0]= saveClientResult;
        SaveResult[] clientManagementSaveResults = new SaveResult[1];
        SaveResult saveClientManagementResult = new SaveResult();
        saveClientManagementResult.setId(clientManagementId);
        saveClientManagementResult.setSuccess(true);
        clientManagementSaveResults[0]= saveClientManagementResult;
        when(salesforceSoapService.createObjectsInSalesforce(any())).thenReturn(clientSaveResults).thenReturn(clientManagementSaveResults);

        assertEquals(1, clients.size());
        assertEquals(Client.Status.UNKNOWN, clients.get(0).getClientStatus());
        assertEquals(Client.Status.UNKNOWN, clients.get(0).getClientManagementStatus());
        assertNull(clients.get(0).getId());
        assertNull(clients.get(0).getClientManagementId());
        salesforceService.pushClientInformationToOrganisation(organisationSalesforceInformation, clients);
        assertEquals(1, clients.size());
        assertEquals(clientId, clients.get(0).getId());
        assertEquals(clientManagementId, clients.get(0).getClientManagementId());

        assertEquals(Client.Status.CREATED, clients.get(0).getClientStatus());
        assertEquals(Client.Status.CREATED, clients.get(0).getClientManagementStatus());
    }

    @DisplayName("Test Creating Multiple New Client And Multiple New Client Management In Salesforce")
    @Test
    void testCreatingMultipleNewClientAndMultipleNewClientManagement() throws ConnectionException {
        final String clientId= "a1wO0000002Z48iIAC";
        final String clientManagementId= "a1wO0000222Z48iIAC";
        OrganisationSalesforceInformation organisationSalesforceInformation = new OrganisationSalesforceInformation("fdsa", "");
        List<Client > clients = new ArrayList<>();
        Client newClient1 = new Client();
        newClient1.setFirstName("Lionel");
        newClient1.setLastName("Messi");
        newClient1.setEmail("messi@gmail.com");
        newClient1.setLandline("0360907766");
        newClient1.setMobile("0429008390");
        newClient1.setTranscriptNo("transcriptId1");
        newClient1.setInformation("info1");
        clients.add(newClient1);
        Client newClient2 = new Client();
        newClient2.setFirstName("Micheal");
        newClient2.setLastName("Owen");
        newClient2.setEmail("owen@gmail.com");
        newClient2.setLandline("0245678962");
        newClient2.setTranscriptNo("transcriptId2");
        newClient2.setInformation("info2");
        clients.add(newClient2);

        QueryResult clientRecordTypeResult = new QueryResult();
        clientRecordTypeResult.setDone(true);
        clientRecordTypeResult.setSize(1);
        SObject sObject = new SObject();
        sObject.setId("01228000000LLXQAA4");
        SObject[] sObjects = new SObject[1];
        sObjects[0] = sObject;
        clientRecordTypeResult.setRecords(sObjects);

        QueryResult noResult = new QueryResult();
        noResult.setDone(true);
        noResult.setSize(0);
        when(salesforceSoapService.query(any())).thenReturn(clientRecordTypeResult).thenReturn(noResult);
        SaveResult[] clientSaveResults = new SaveResult[2];
        SaveResult saveClientResult = new SaveResult();
        saveClientResult.setId(clientId);
        saveClientResult.setSuccess(true);
        clientSaveResults[0]= saveClientResult;

        SaveResult saveClientResult2 = new SaveResult();
        saveClientResult2.setId(clientId);
        saveClientResult2.setSuccess(true);
        clientSaveResults[1]= saveClientResult2;
        SaveResult[] clientManagementSaveResults = new SaveResult[2];
        SaveResult saveClientManagementResult = new SaveResult();
        saveClientManagementResult.setId(clientManagementId);
        saveClientManagementResult.setSuccess(true);
        clientManagementSaveResults[0]= saveClientManagementResult;

        SaveResult saveClientManagementResult2 = new SaveResult();
        saveClientManagementResult2.setId(clientManagementId);
        saveClientManagementResult2.setSuccess(true);
        clientManagementSaveResults[1]= saveClientManagementResult2;
        when(salesforceSoapService.createObjectsInSalesforce(any())).thenReturn(clientSaveResults).thenReturn(clientManagementSaveResults);

        assertEquals(2, clients.size());

        assertEquals(Client.Status.UNKNOWN, clients.get(0).getClientStatus());
        assertEquals(Client.Status.UNKNOWN, clients.get(0).getClientManagementStatus());
        assertNull(clients.get(0).getId());
        assertNull(clients.get(0).getClientManagementId());
        salesforceService.pushClientInformationToOrganisation(organisationSalesforceInformation, clients);
        assertEquals(2, clients.size());
        assertEquals(clientId, clients.get(0).getId());
        assertEquals(clientManagementId, clients.get(0).getClientManagementId());

        assertEquals(Client.Status.CREATED, clients.get(0).getClientStatus());
        assertEquals(Client.Status.CREATED, clients.get(0).getClientManagementStatus());
        assertEquals(clientId, clients.get(1).getId());
        assertEquals(clientManagementId, clients.get(1).getClientManagementId());

        assertEquals(Client.Status.CREATED, clients.get(1).getClientStatus());
        assertEquals(Client.Status.CREATED, clients.get(1).getClientManagementStatus());
    }
}
