package com.ctc.property.meshintegration.Util;

import com.ctc.property.meshintegration.entity.MeshIntegrationRequest;
import com.ctc.property.meshintegration.entity.ProjectDetail;
import com.ctc.property.meshintegration.salesforce.Client;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.*;

public class MeshIntegrationUtil {
    private static final Logger LOG = Logger.getLogger(MeshIntegrationUtil.class.getName());

    public static Optional<MeshIntegrationRequest> toMeshIntegrationRequest(final String json){
        LOG.info("Converting json to mesh integration request");
        MeshIntegrationRequest requestMessage = new MeshIntegrationRequest();
        try {
            JSONObject obj = new JSONObject(json);
            requestMessage.setOrgId(obj.getString("orgId"));
            requestMessage.setNamespace(obj.getString("namespace"));
            requestMessage.setFromDateTime(obj.getLong("fromDateTime"));
            JSONArray arr = obj.getJSONArray("projectDetails");
            for (int i = 0; i < arr.length(); i++) {
                ProjectDetail projectDetail = new ProjectDetail();
                projectDetail.setProjectId(arr.getJSONObject(i).getString("projectId"));
                projectDetail.setUrl(arr.getJSONObject(i).getString("url"));
                projectDetail.setKey(arr.getJSONObject(i).getString("key"));
                projectDetail.setPass(arr.getJSONObject(i).getString("pass"));
                if (projectDetail.isEligible())
                    requestMessage.getProjectDetails().add(projectDetail);
            }
        }
        catch (JSONException e){
            LOG.error("Fail to convert json mesh integration request message:"+json+" due to "+e);
            return Optional.empty();
        }
        if (requestMessage.isEligible())
            return Optional.of(requestMessage);
        else
            return Optional.empty();
    }

    public static String toDateTimeStringFormatUsedByMeshAPI(final long fromDatetime){
        Date date = new Date(fromDatetime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    public static Set<String> retrieveTranscriptIds(final String json){
        LOG.info("Retrieving transcript ids from mesh");
        LOG.info("Json data received from mesh\n"+json);
        final Set<String> transcriptIds = new HashSet<>();
        try {
            JSONObject obj = new JSONObject(json);
            JSONObject response = (JSONObject) obj.get("response");
            JSONArray arr = (JSONArray) response.get("data");
            for (int i = 0; i < arr.length(); i++) {
                JSONObject jsonObject = (JSONObject) arr.get(i);
                transcriptIds.add((String) jsonObject.get("transcriptNo"));
            }
        }catch (JSONException e){
            LOG.error("Fail to retrieve transcript Id due to "+e);
        }
        return transcriptIds;
    }

    public static List<Client> toClients(List<String> jsons) {
        LOG.info("Converting Json datas to clients");
        List<Client> clients = new ArrayList<>();
        jsons.forEach(json ->
            toClient(json).filter(Client::isValid).map(clients::add)
        );
        clients.forEach(client -> LOG.info(client.toString()));
        LOG.info(clients.size() +" clients are retrieved successfully from mesh");
        return clients;
    }

    private static Optional<Client> toClient(String json){
        LOG.info("Converting json to client");
        LOG.info("Json\n"+json);
        try {
            JSONObject obj = new JSONObject(json);
            JSONObject response = (JSONObject) obj.get("response");
            JSONArray arr = (JSONArray) response.get("data");
            Client client = new Client();
            StringBuilder information = new StringBuilder();
            for (int i = 0; i < arr.length(); i++) {
                JSONObject jsonObject = (JSONObject) arr.get(i);
                String dataName = (String) jsonObject.get("dataName");
                client.setTranscriptNo((String) jsonObject.get("transcriptNo"));
                if ("Name".equalsIgnoreCase(dataName)) {
                    client.setFirstName(convertDataValueToString(jsonObject));
                } else if ("Email Address".equalsIgnoreCase(dataName)) {
                    client.setEmail(convertDataValueToString(jsonObject));
                } else if ("Phone".equalsIgnoreCase(dataName)) {
                    client.setLandline(convertDataValueToString(jsonObject));
                } else if ("Phone (Mobile)".equalsIgnoreCase(dataName)) {
                    client.setMobile(convertDataValueToString(jsonObject));
                }
                information.append(dataName).append(":").append(convertDataValueToString(jsonObject)).append("\n");
            }
            client.setInformation(information.toString());
            return Optional.of(client);
        }
        catch (JSONException e){
            LOG.error("Fail to convert json to client due to "+e);
            return Optional.empty();
        }
    }

    private static String convertDataValueToString(JSONObject jsonObject){
        LOG.info("Converting data value from json object to string");
        try {
            Object dataValue = jsonObject.get("dataValue");
            if (dataValue != null && !dataValue.toString().equalsIgnoreCase("null"))
                return dataValue.toString();
        } catch (JSONException e){
            LOG.error("Fail to convert data value from json object to string due to "+e);
        }
        return null;
    }
}
