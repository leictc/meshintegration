package com.ctc.property.meshintegration.scheduler;

import com.ctc.property.meshintegration.service.MeshIntegrationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class MeshIntegrationScheduler {
    private static final Logger LOG = Logger.getLogger(MeshIntegrationScheduler.class.getName());

    private final MeshIntegrationService meshIntegrationService;

    @Autowired
    public MeshIntegrationScheduler(MeshIntegrationService meshIntegrationService) {
        this.meshIntegrationService = meshIntegrationService;
    }

    @Scheduled(cron = "${scheduler.cron}")
    public void processRequests() {
        LOG.info("MeshIntegrationScheduler is processing requests from salesforce");
        meshIntegrationService.process();
    }

    private void processRequestsByMutiThreads(){
        ExecutorService executor = Executors.newFixedThreadPool(4);
        for (int i = 0; i < 4; i++) {
            executor.execute(meshIntegrationService::process);
        }
        executor.shutdown();
        LOG.info("Finished all threads");

    }
}
