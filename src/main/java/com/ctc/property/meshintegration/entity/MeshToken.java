package com.ctc.property.meshintegration.entity;

import java.time.LocalDateTime;

public class MeshToken {
    private String token;
    private LocalDateTime expires;

    public MeshToken(String token, LocalDateTime expires){
        this.token = token;
        this.expires = expires;
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getExpires() {
        return expires;
    }

    @Override
    public String toString() {
        return "MeshToken{" +
                "token='" + token + '\'' +
                ", expires=" + expires +
                '}';
    }
}
