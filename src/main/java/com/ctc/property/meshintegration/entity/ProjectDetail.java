package com.ctc.property.meshintegration.entity;

import org.springframework.util.StringUtils;

public class ProjectDetail {
    private String projectId;
    private String url;
    private String key;
    private String pass;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "ProjectDetail{" +
                "projectId='" + projectId + '\'' +
                ", url='" + url + '\'' +
                ", key='" + key + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isEligible() {
        return !StringUtils.isEmpty(this.projectId) && !StringUtils.isEmpty(this.url) && !StringUtils.isEmpty(this.key) && !StringUtils.isEmpty(this.pass);
    }

}
