package com.ctc.property.meshintegration.entity;

import java.util.Objects;

public class OrganisationSalesforceInformation {
    private String orgId;
    private String namespace;

    public OrganisationSalesforceInformation(final String orgId, final String namespace){
        this.orgId = orgId;
        this.namespace = namespace;
    }

    public String getOrgId() {
        return orgId;
    }

    public String getNamespace() {
        return namespace;
    }

    @Override
    public String toString() {
        return "OrganisationSalesforceInformation{" +
                "orgId='" + orgId + '\'' +
                ", namespace='" + namespace + '\'' +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganisationSalesforceInformation that = (OrganisationSalesforceInformation) o;
        return Objects.equals(orgId, that.orgId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orgId);
    }

}
