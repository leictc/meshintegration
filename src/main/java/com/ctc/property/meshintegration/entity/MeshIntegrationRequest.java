package com.ctc.property.meshintegration.entity;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class MeshIntegrationRequest {
    private String orgId;
    private long fromDateTime;
    private long toDateTime;
    private String namespace="";
    private List<ProjectDetail> projectDetails = new ArrayList<>();

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public long getFromDateTime() {
        return fromDateTime;
    }

    public void setFromDateTime(long fromDateTime) {
        this.fromDateTime = fromDateTime;
    }

    public long getToDateTime() {
        return toDateTime;
    }

    public void setToDateTime(long toDateTime) {
        this.toDateTime = toDateTime;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public List<ProjectDetail> getProjectDetails(){
        return this.projectDetails;
    }

    public boolean isEligible() {
        return !StringUtils.isEmpty(this.orgId) && !this.projectDetails.isEmpty() && this.fromDateTime != 0;
    }
}
