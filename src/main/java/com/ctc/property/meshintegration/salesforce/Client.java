package com.ctc.property.meshintegration.salesforce;

import com.sforce.soap.partner.sobject.SObject;
import org.springframework.util.StringUtils;

import java.util.Objects;

/**
 * It has client and client management information together
 * @author Lei Zhu
 */
public class Client {
    private String Id;
    private String firstName;
    private String lastName= ".";
    private String email;
    private String mobile;
    private String landline;
    private String transcriptNo;
    private String clientManagementId;
    private String information;
    private Status clientStatus = Status.UNKNOWN;
    private String clientErrorMessage;
    private Status clientManagementStatus = Status.UNKNOWN;
    private String clientManagementErrorMessage;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandline() {
        return landline;
    }

    public void setLandline(String landline) {
        this.landline = landline;
    }

    public String getTranscriptNo() {
        return transcriptNo;
    }

    public void setTranscriptNo(String transcriptNo) {
        this.transcriptNo = transcriptNo;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getClientManagementId() {
        return clientManagementId;
    }

    public void setClientManagementId(String clientManagementId) {
        this.clientManagementId = clientManagementId;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public boolean isValid(){
        return (!StringUtils.isEmpty(email) || !StringUtils.isEmpty(mobile) || !StringUtils.isEmpty(landline)) && !StringUtils.isEmpty(firstName) && !StringUtils.isEmpty(transcriptNo);
    }

    public SObject toClientSObject(final String clientRecordTypeId){
        SObject sobj = new SObject();
        sobj.setType("Account");
        sobj.setField("FirstName", getFirstName());
        sobj.setField("LastName", getLastName());
        sobj.setField("PersonEmail", getEmail());
        sobj.setField("PersonMobilePhone", getMobile());
        sobj.setField("PersonHomePhone", getLandline());
        sobj.setField("RecordTypeId",clientRecordTypeId);
        return sobj;
    }

    public SObject toClientManagementSObject(final String namespace) {
        SObject sobj = new SObject();
        sobj.setType(namespace+"Client_Management__c");
        sobj.setField(namespace+"Client__c", getId());
        sobj.setField(namespace+"Enliven_Transcript_ID__c", getTranscriptNo());
        sobj.setField(namespace+"Client_Comments__c", getInformation());
        return sobj;
    }

    public SObject toUpdateClientManagementSObject(final String namespace) {
        SObject sobj = new SObject();
        sobj.setType(namespace+"Client_Management__c");
        sobj.setField("Id", getClientManagementId());
        sobj.setField(namespace+"Client_Comments__c", getInformation());
        return sobj;
    }

    public Status getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(Status clientStatus) {
        this.clientStatus = clientStatus;
    }

    public Status getClientManagementStatus() {
        return clientManagementStatus;
    }

    public void setClientManagementStatus(Status clientManagementStatus) {
        this.clientManagementStatus = clientManagementStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(email, client.email) &&
                Objects.equals(mobile, client.mobile) &&
                Objects.equals(landline, client.landline);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, mobile, landline);
    }

    @Override
    public String toString() {
        return " Client Id :" + getId() +
                " Client Management Id :" + getClientManagementId() +
                " FirstName :" + getFirstName() +
                " LastName :" + getLastName() +
                " Transcript ID :" + getTranscriptNo() +
                " Email :" + getEmail() +
                " Landline :" + getLandline() +
                " Mobile :" + getMobile() +
                " Client Status :" + getClientStatus() +
                " Client Error Message :" + getClientErrorMessage() +
                " Client Management Status :" + getClientManagementStatus() +
                " Client Management Error Message :" + getClientManagementErrorMessage();
    }

    public String getClientErrorMessage() {
        return clientErrorMessage;
    }

    public void setClientErrorMessage(String clientErrorMessage) {
        this.clientErrorMessage = clientErrorMessage;
    }

    public String getClientManagementErrorMessage() {
        return clientManagementErrorMessage;
    }

    public void setClientManagementErrorMessage(String clientManagementErrorMessage) {
        this.clientManagementErrorMessage = clientManagementErrorMessage;
    }

    public enum Status{
        UNKNOWN, UPDATED, CREATED, EXISTED, FAILED_CREATION, FAILED_UPDATE
    }
}
