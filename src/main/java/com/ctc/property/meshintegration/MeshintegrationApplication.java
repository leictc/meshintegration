package com.ctc.property.meshintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MeshintegrationApplication {
	public static void main(String[] args) {
		SpringApplication.run(MeshintegrationApplication.class, args);
	}

}

