package com.ctc.property.meshintegration.Exception;

/**
 * The MeshIntegrationException wraps all unchecked standard Java exception and enriches them with a custom error message
 * @author Lei Zhu
 */
public class MeshIntegrationException extends Exception  {

    public MeshIntegrationException(String message) {
        super(message);
    }

    public MeshIntegrationException(String message, Throwable t) {
        super(message, t);
    }
}
