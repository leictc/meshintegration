package com.ctc.property.meshintegration.service;

import com.amazonaws.services.sqs.model.Message;

import java.util.List;

public interface MessageService {
    List<Message> receiveMessage();

    void deleteMessage(Message message);
}
