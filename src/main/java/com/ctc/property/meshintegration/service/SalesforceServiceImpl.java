package com.ctc.property.meshintegration.service;

import com.ctc.property.meshintegration.Exception.MeshIntegrationException;
import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.salesforce.Client;
import com.sforce.soap.partner.Error;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class SalesforceServiceImpl implements SalesforceService {
    private static final Logger LOG = Logger.getLogger(SalesforceServiceImpl.class.getName());
    private static final int NUMBER_OF_TRANSCRIPT_PROCESSED = 10;

    @Autowired
    private SalesforceSoapService salesforceSoapService;

    @Override
    public void pushClientInformationToOrganisation(final OrganisationSalesforceInformation organisationSalesforceInformation, final List<Client> clientList) {
        LOG.info("Pushing "+clientList.size()+" clients to organisation :"+organisationSalesforceInformation.getOrgId()+" on salesforce");
        try {
            connectToOrganisation(organisationSalesforceInformation.getOrgId());
            final String clientRecordTypeId = retrieveClientRecordTypeId();
            for (List<Client> clients : divideAListToLists(clientList)) {
                LOG.info("Pushing "+ clients.size()+" clients to organisation");
                updateClientManagementInformationInClients(clients, retrieveClientManagements(toSearchClientManagementQuery(organisationSalesforceInformation.getNamespace(), clients)));
                updateClientManagementInformationInClients(clients, updateClientManagements(organisationSalesforceInformation.getNamespace(), clients));
                updateClientInformationInClients(clients, retrieveClients(toSearchClientQuery(clients)));
                updateClientInformationInClients(clients, createClients(clients, clientRecordTypeId));
                updateClientManagementInformationInClients(clients, createClientManagements(organisationSalesforceInformation.getNamespace(), clients));
                clients.forEach(client -> LOG.info(String.valueOf(client)));
            }
        } catch (MeshIntegrationException e) {
            LOG.error("Fail to push client information to organisation :"+organisationSalesforceInformation+" on salesforce due to "+e);
        }
    }

    private String retrieveClientRecordTypeId() throws MeshIntegrationException {
        LOG.info("Retrieving client record type id");
        final QueryResult queryResult;
        try {
            queryResult = this.salesforceSoapService.query("Select Id From RecordType where DeveloperName='Individual_Clients' limit 1");
        } catch (ConnectionException e) {
            throw new MeshIntegrationException("Fail to retrieve record type for client ", e);
        }
        if (queryResult.getSize() > 0) {
            String id = queryResult.getRecords()[0].getId();
            if (!StringUtils.isEmpty(id))
                return id;
        }
        throw new MeshIntegrationException("The client record type name does not exist");
    }

    private void connectToOrganisation(String orgId) throws MeshIntegrationException {
        LOG.info("Connecting to organisation:"+orgId);
        try {
            this.salesforceSoapService.connectToOrganisation(orgId);
        } catch (ConnectionException e) {
            throw new MeshIntegrationException("Fail to connect to organisation :"+orgId+" on salesforce", e);
        }
    }

    private Set<Client> retrieveClients(final String queryStr) {
        LOG.info("Retrieving clients in salesforce");
        final Set<Client> existingClients = new HashSet<>();
        try {
            if (!StringUtils.isEmpty(queryStr))
                existingClients.addAll(retrieveExistingClients(this.salesforceSoapService.query(queryStr)));
        } catch (ConnectionException e) {
            LOG.error(" Fail to execute search query to salesforce due to "+e);
        }
        return existingClients;
    }

    private String toSearchClientManagementQuery(final String namespace, final Collection<Client> clients){
        LOG.info("Converting to search client management query");
        StringBuilder query =  new StringBuilder();
        query.append("select Id, ").append(namespace).append("Client__c, ").append(namespace).append("Enliven_Transcript_ID__c from ").append(namespace).append("Client_Management__c where ").append(namespace).append("Enliven_Transcript_ID__c in ( ");
        AtomicBoolean result = new AtomicBoolean(false);
        clients.stream().filter(client -> !StringUtils.isEmpty(client.getTranscriptNo())).forEach(client -> {
            query.append("\'").append(client.getTranscriptNo()).append("\' ,");
            result.set(true);
        });
        query.deleteCharAt(query.toString().length()-1);
        query.append(")");

        if(result.get())
            return query.toString();
        return "";

    }

    private List<Client> retrieveClientManagements(final String queryStr){
        LOG.info("Retrieving client managements in salesforce");
        LOG.info("Retrieving client managements Query:\n"+queryStr);
        final List<Client> existingClientManagements = new ArrayList<>();
        try {
            existingClientManagements.addAll(retrieveExistingClientManagements (this.salesforceSoapService.query(queryStr)));
        } catch (ConnectionException e) {
            LOG.info(" Fail to execute search query to salesforce due to "+e);
        }
        return existingClientManagements;
    }

    private String toSearchClientQuery(final Collection<Client> clients){
        StringBuilder searchQuery =  new StringBuilder();
        searchQuery.append("select Id, FirstName, LastName, PersonEmail, PersonMobilePhone, PersonHomePhone from Account where ");
        String emailCondition = getPersonEmailQueryCondition(clients);
        String mobileCondition = getPersonMobileQueryCondition(clients);
        String homePhoneCondition = getPersonHomePhoneQueryCondition(clients);
        if (!StringUtils.isEmpty(emailCondition)&& !StringUtils.isEmpty(mobileCondition) && !StringUtils.isEmpty(homePhoneCondition))
            searchQuery.append(emailCondition).append(" or ").append(mobileCondition).append(" or ").append(homePhoneCondition);
        else if (!StringUtils.isEmpty(emailCondition) && !StringUtils.isEmpty(mobileCondition))
            searchQuery.append(emailCondition).append(" or ").append(mobileCondition);
        else if ( !StringUtils.isEmpty(mobileCondition ) && !StringUtils.isEmpty(homePhoneCondition))
            searchQuery.append(mobileCondition).append(" or ").append(homePhoneCondition);
        else if (!StringUtils.isEmpty(emailCondition) && !StringUtils.isEmpty(homePhoneCondition))
            searchQuery.append(emailCondition).append(" or ").append(homePhoneCondition);
        else if (!StringUtils.isEmpty(emailCondition))
            searchQuery.append(emailCondition);
        else if (!StringUtils.isEmpty(homePhoneCondition ))
            searchQuery.append(homePhoneCondition);
        else if (!StringUtils.isEmpty(mobileCondition))
            searchQuery.append(mobileCondition);
        else
            return null;
        searchQuery.append(" order by createdDate desc");
        return searchQuery.toString();

    }

    private String getPersonEmailQueryCondition(final Collection<Client> clients){
        StringBuilder emailCondition = new StringBuilder();
        emailCondition.append("PersonEmail in (");
        AtomicBoolean result = new AtomicBoolean(false);
        clients.stream().filter(client -> !StringUtils.isEmpty(client.getEmail()) && client.getClientStatus().equals(Client.Status.UNKNOWN)).forEach(client -> {
            emailCondition.append("\'").append(client.getEmail()).append("\' ,");
            result.set(true);
        });
        emailCondition.deleteCharAt(emailCondition.toString().length()-1);
        emailCondition.append(")");

        if(result.get())
            return emailCondition.toString();

        return "";
    }

    private String getPersonMobileQueryCondition(final Collection<Client> clients){
        StringBuilder mobileCondition = new StringBuilder();
        mobileCondition.append("PersonMobilePhone in (");
        AtomicBoolean result = new AtomicBoolean(false);
        clients.stream().filter(client -> !StringUtils.isEmpty(client.getMobile())  && client.getClientStatus().equals(Client.Status.UNKNOWN) ).forEach(client -> {
            mobileCondition.append("\'").append(client.getMobile()).append("\' ,");
            result.set(true);
        });
        mobileCondition.deleteCharAt(mobileCondition.toString().length()-1);
        mobileCondition.append(")");

        if(result.get())
            return mobileCondition.toString();

        return "";
    }

    private String getPersonHomePhoneQueryCondition(final Collection<Client> clients){
        StringBuilder homephoneCondition = new StringBuilder();
        homephoneCondition.append("PersonHomePhone in (");
        AtomicBoolean result = new AtomicBoolean(false);
        clients.stream().filter(client -> !StringUtils.isEmpty(client.getLandline()) && client.getClientStatus().equals(Client.Status.UNKNOWN)).forEach(client -> {
            homephoneCondition.append("\'").append(client.getLandline()).append("\' ,");
            result.set(true);
        });
        homephoneCondition.deleteCharAt(homephoneCondition.toString().length()-1);
        homephoneCondition.append(")");

        if(result.get())
            return homephoneCondition.toString();

        return "";
    }
    private List<Client> retrieveExistingClientManagements(final QueryResult queryResult) {
        final List<Client> clientList = new ArrayList<>();
        for (int i=0; i< queryResult.getRecords().length;i++){
            Client client = new Client();
            client.setClientManagementId((String) queryResult.getRecords()[i].getField("Id"));
            client.setId((String) queryResult.getRecords()[i].getField("Client__c"));
            client.setTranscriptNo((String) queryResult.getRecords()[i].getField("Enliven_Transcript_ID__c"));
            client.setClientManagementStatus(Client.Status.EXISTED);
            clientList.add(client);
        }
        return clientList;
    }


    private Set<Client> retrieveExistingClients(final QueryResult queryResult){
        final Set<Client> existingClients = new HashSet<>();
        for (int i=0; i< queryResult.getRecords().length;i++){
            Client client = new Client();
            client.setId((String) queryResult.getRecords()[i].getField("Id"));
            client.setFirstName((String) queryResult.getRecords()[i].getField("FirstName"));
            client.setLastName((String) queryResult.getRecords()[i].getField("LastName"));
            client.setEmail((String) queryResult.getRecords()[i].getField("PersonEmail"));
            client.setLandline((String) queryResult.getRecords()[i].getField("PersonHomePhone"));
            client.setMobile((String) queryResult.getRecords()[i].getField("PersonMobilePhone"));
            client.setClientStatus(Client.Status.EXISTED);
            existingClients.add(client);
        }
        return existingClients;
    }

    private List<Client> createClients(final List<Client> clientList, final String clientRecordTypeId) {
        LOG.info("Creating clients in salesforce");
        final List<SObject> sObjectList = new ArrayList<>();
        final List<Client> newClientList = clientList.stream().filter(client -> client.getClientStatus().equals(Client.Status.UNKNOWN)).collect(Collectors.toList());
        newClientList.forEach(client -> sObjectList.add(client.toClientSObject(clientRecordTypeId)));

        if (!sObjectList.isEmpty()) {
            try {
                SaveResult[] saveResults = this.salesforceSoapService.createObjectsInSalesforce(sObjectList.toArray(new SObject[]{}));
                for (int i=0; i<saveResults.length; i++){
                    if(saveResults[i].isSuccess()) {
                        newClientList.get(i).setId(saveResults[i].getId());
                        newClientList.get(i).setClientStatus(Client.Status.CREATED);
                        LOG.info(" Client created successfully:\n"+saveResults[i]);
                    }
                    else {
                        newClientList.get(i).setClientStatus(Client.Status.FAILED_CREATION);
                        StringBuilder errorMsg = new StringBuilder();
                        LOG.info("Fail to create client :\n" + saveResults[i]);
                        for (int j = 0; j < saveResults[i].getErrors().length; j++) {
                            Error err = saveResults[i].getErrors()[j];
                            LOG.info("Errors were found on item " + j);
                            LOG.info("Error code: " +
                                    err.getStatusCode().toString());
                            LOG.info("Error message: " + err.getMessage());
                            errorMsg.append(err.getMessage());
                        }
                        newClientList.get(i).setClientErrorMessage(errorMsg.toString());
                    }
                }
            } catch (ConnectionException e) {
                LOG.error("Fail to create clients in salesforce due to " + e);
            }
        }
        return newClientList;
    }

    private List<Client> createClientManagements(final String namespace, final Collection<Client> clientList) {
        LOG.info("Creating client managements in salesforce");
        final List<SObject> sObjectList = new ArrayList<>();
        final List<Client> newClientManagementList = clientList.stream().filter(client -> StringUtils.isEmpty(client.getClientManagementId()) && !StringUtils.isEmpty(client.getId())).collect(Collectors.toList());
        newClientManagementList.forEach(clientManagement -> sObjectList.add(clientManagement.toClientManagementSObject(namespace)));

        if (!sObjectList.isEmpty()) {
            try {
                SaveResult[] saveResults = this.salesforceSoapService.createObjectsInSalesforce(sObjectList.toArray(new SObject[]{}));
                for (int i=0; i<saveResults.length; i++){
                    if(saveResults[i].isSuccess()) {
                        newClientManagementList.get(i).setClientManagementStatus(Client.Status.CREATED);
                        newClientManagementList.get(i).setClientManagementId(saveResults[i].getId());
                        LOG.info(" Client management created successfully:\n"+saveResults[i]);
                    }
                    else {
                        newClientManagementList.get(i).setClientStatus(Client.Status.FAILED_CREATION);
                        StringBuilder errorMsg = new StringBuilder();
                        LOG.info("Fail to create client management :\n" + saveResults[i]);
                        for (int j = 0; j < saveResults[i].getErrors().length; j++) {
                            Error err = saveResults[i].getErrors()[j];
                            LOG.info("Errors were found on item " + j);
                            LOG.info("Error code: " +err.getStatusCode().toString());
                            LOG.info("Error message: " + err.getMessage());
                            errorMsg.append(err.getMessage());
                        }
                        newClientManagementList.get(i).setClientManagementErrorMessage(errorMsg.toString());
                    }
                }
            } catch (ConnectionException e) {
                LOG.error("Fail to create client managements in salesforce due to " + e);
            }
        }
        return newClientManagementList;
    }

    private List<Client> updateClientManagements(final String namespace, final Collection<Client> clients) {
        LOG.info("Updating client managements in salesforce");
        final List<SObject> sObjectList = new ArrayList<>();
        final List<Client> existingClientManagementList = clients.stream().filter(client ->!StringUtils.isEmpty(client.getClientManagementId())).collect(Collectors.toList());
        existingClientManagementList.forEach(clientManagement -> sObjectList.add(clientManagement.toUpdateClientManagementSObject(namespace)));

        if (!sObjectList.isEmpty()) {
            try {
                SaveResult[] saveResults = this.salesforceSoapService.updateObjectsInSalesforce(sObjectList.toArray(new SObject[]{}));
                for (int i=0; i<saveResults.length; i++){
                    if (saveResults[i].isSuccess()) {
                        existingClientManagementList.get(i).setClientManagementStatus(Client.Status.UPDATED);
                        LOG.info("Client management updated successfully :\n" + saveResults[i]);
                    }
                    else {
                        existingClientManagementList.get(i).setClientStatus(Client.Status.FAILED_UPDATE);
                        StringBuilder errorMsg = new StringBuilder();
                        LOG.info("Fail to update client management:\n" + saveResults[i]);
                        for (int j = 0; j < saveResults[i].getErrors().length; j++) {
                            Error err = saveResults[i].getErrors()[j];
                            LOG.info("Errors were found on item " + j);
                            LOG.info("Error code: " +
                                    err.getStatusCode().toString());
                            LOG.info("Error message: " + err.getMessage());
                            errorMsg.append(err.getMessage());
                        }
                        existingClientManagementList.get(i).setClientManagementErrorMessage(errorMsg.toString());

                    }
                }
            } catch (ConnectionException e) {
                LOG.error("Fail to update client managements in salesforce due to " + e);
            }
        }
        return existingClientManagementList;
    }


    private Collection<List<Client>> divideAListToLists(final Collection<Client> clients){
        final AtomicInteger counter = new AtomicInteger();
        return clients.stream().collect(Collectors.groupingBy(it -> counter.getAndIncrement() / NUMBER_OF_TRANSCRIPT_PROCESSED)).values();
    }

    private void updateClientInformationInClients(final Collection<Client> newClientList, final Collection<Client> existingClientList){
        newClientList.forEach(client -> {
            for (Client existingClient : existingClientList) {
                if ((client.getMobile() != null && existingClient.getMobile() != null && client.getMobile().equalsIgnoreCase(existingClient.getMobile())) ||
                        (client.getLandline() != null && existingClient.getLandline() != null  && client.getLandline().equalsIgnoreCase(existingClient.getLandline()))  ||
                        (client.getEmail() != null && existingClient.getEmail() != null
                                && client.getEmail().equalsIgnoreCase(existingClient.getEmail()))) {
                    client.setId(existingClient.getId());
                    client.setClientStatus(existingClient.getClientStatus());
                    client.setClientErrorMessage(existingClient.getClientErrorMessage());
                    client.setClientManagementErrorMessage(existingClient.getClientManagementErrorMessage());
                    client.setClientStatus(existingClient.getClientStatus());
                }
            }
        });
    }

    private void updateClientManagementInformationInClients(final Collection<Client> clientList, final Collection<Client> existingClientManagementList) {
        clientList.forEach(client -> existingClientManagementList.stream().filter(existingClientManagement -> client.getTranscriptNo() != null && existingClientManagement.getTranscriptNo() != null
                && client.getTranscriptNo().equalsIgnoreCase(existingClientManagement.getTranscriptNo()))
                .forEach(existingClientManagement -> {
                    client.setClientManagementId(existingClientManagement.getClientManagementId());
                    client.setClientManagementStatus(existingClientManagement.getClientManagementStatus());
                    client.setClientErrorMessage(existingClientManagement.getClientErrorMessage());
                    client.setClientManagementErrorMessage(existingClientManagement.getClientManagementErrorMessage());
                    if ((existingClientManagement.getClientManagementStatus().equals(Client.Status.UPDATED)|| existingClientManagement.getClientManagementStatus().equals(Client.Status.FAILED_UPDATE) ) && client.getClientStatus().equals(Client.Status.UNKNOWN))
                        client.setClientStatus(Client.Status.EXISTED);
        }));
    }

}
