package com.ctc.property.meshintegration.service;

import com.ctc.salesforce.enhanced.adaptor.SalesforceAgent;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import org.springframework.stereotype.Service;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class SalesforceSoapServiceImpl implements  SalesforceSoapService {
    private PartnerConnection partnerConnection;

    @Override
    public SaveResult[] createObjectsInSalesforce(SObject[] sObjects) throws ConnectionException {
        return this.partnerConnection.create(sObjects);
    }

    @Override
    public SaveResult[] updateObjectsInSalesforce(SObject[] sObjects) throws ConnectionException {
        return this.partnerConnection.update(sObjects);
    }

    @Override
    public QueryResult query(String query) throws ConnectionException {
        return this.partnerConnection.query(checkNotNull(query," Query is null"));
    }

    @Override
    public void connectToOrganisation(String orgId) throws ConnectionException {
        this.partnerConnection = SalesforceAgent.getConnectionByOAuth(checkNotNull(orgId," Organisation id is null"));
    }
}
