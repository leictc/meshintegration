package com.ctc.property.meshintegration.service;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public interface SalesforceSoapService {
    SaveResult[] createObjectsInSalesforce(SObject[] sObjects) throws ConnectionException;

    SaveResult[] updateObjectsInSalesforce(SObject[] sObjects) throws ConnectionException;

    QueryResult query(String query) throws ConnectionException;

    void connectToOrganisation(String orgId) throws ConnectionException;
}
