package com.ctc.property.meshintegration.service;

import com.ctc.property.meshintegration.entity.MeshToken;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class MeshAPIServiceImpl implements MeshAPIService{
    private static final Logger LOG = Logger.getLogger(MeshAPIServiceImpl.class.getName());
    private static final String KEY ="key";
    private static final String PASS ="pass";
    private static final String TOKEN ="token";
    private static final String EXPIRES ="expires";
    private static final String TOKEN_URI ="/api/token";
    private static final String TRANSCRIPT_URI ="/api/transcript-data-sys";
    private static final String AUTHORIZATION ="Authorization";
    private static final String TNS ="tns";
    private String url;
    private RestTemplate rt;

    @Override
    public void initialise(String url) {
        LOG.info("Initialising Mesh Service");
        this.url = checkNotNull(url, "Url is null");
        this.rt = new RestTemplate();
    }

    @Override
    public Optional<MeshToken> retrieveToken(String key, String pass) {
        final String uriString = url+TOKEN_URI;
        LOG.info("Retrieving token from "+uriString);
        final URI uri;
        try {
            uri = new URI(uriString);
        } catch (URISyntaxException e) {
            LOG.error("Fail to initialise the URI of mesh token api ",e);
            return Optional.empty();
        }
        // create request body
        final JSONObject jsonRequest = new JSONObject();
        jsonRequest.put(KEY, checkNotNull(key, "Key is null"));
        jsonRequest.put(PASS, checkNotNull(pass, "Pass is null"));

        // set headers
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> httpEntity = new HttpEntity<>(jsonRequest.toString(), headers);

        try {
            // send request and parse result
            ResponseEntity<String> tokenResponse = rt.exchange(uri, HttpMethod.POST, httpEntity, String.class);
            if (tokenResponse.getStatusCode() == HttpStatus.OK) {
                JSONObject tokenJson = new JSONObject(tokenResponse.getBody());
                //60 seconds is reserved as buffer time
                return Optional.of(new MeshToken((String) tokenJson.get(TOKEN), java.time.LocalDateTime.now().plus(Integer.parseInt((String) tokenJson.get(EXPIRES))-60, ChronoUnit.SECONDS)));
            }
            else if (tokenResponse.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                LOG.error("Token Request is unauthorized");
                return Optional.empty();
            }
            else {
                LOG.error("Token Request is unsuccessful");
                return Optional.empty();
            }

        }catch (RestClientException e){
            LOG.error("Fail to receive token response from "+uri+" due to "+e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<String> retrieveData(String token, String fromDateTime) {
        final String uriString = url+TRANSCRIPT_URI;
        LOG.info("Retrieving transcript from "+uriString+" after "+fromDateTime);
        final URI uri;
        try {
            uri = new URI(uriString);
        } catch (URISyntaxException e) {
            LOG.error("Fail to initialise the URI of transcript data sys api due to "+e);
            return Optional.empty();
        }

        //Post request with authentication bearer
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer "+token);

        JSONObject requestJson = new JSONObject();
        requestJson.put("last-updated", fromDateTime);

        HttpEntity<String> httpEntity = new HttpEntity<>(requestJson.toString(),headers);

        try {
            return Optional.ofNullable(rt.postForObject(uri, httpEntity, String.class));
        }
        catch (RestClientException e){
            LOG.error("Fail to retrieve transcripts since "+fromDateTime+" from url:"+url+" due to "+e);
            return Optional.empty();
        }
    }

    @Override
    public List<String> retrieveTranscripts(String token, Set<String> transcriptIds) {
        final List<String> transcripts = new ArrayList<>();
        final String uriString = url+TRANSCRIPT_URI;
        LOG.info("Retrieving transcript from "+uriString);
        final URI uri;
        try {
            uri = new URI(uriString);
        } catch (URISyntaxException e) {
            LOG.error("Fail to initialise the URI of transcript data sys api due to "+e);
            return transcripts;
        }

        //Post request with authentication bearer
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(AUTHORIZATION, "Bearer "+token);

        transcriptIds.forEach(transcriptNo -> {
            JSONObject requestJson = new JSONObject();
            requestJson.put(TNS, transcriptNo);
            HttpEntity<String> httpEntity = new HttpEntity<>(requestJson.toString(),headers);
            try {
                transcripts.add(rt.postForObject(uri, httpEntity, String.class));
            }
            catch (RestClientException e){
                LOG.error("Fail to retrieve transcript:"+transcriptNo+" from url:"+url+" due to "+e);
            }
        });

        return transcripts;
    }
}
