package com.ctc.property.meshintegration.service;

import com.ctc.property.meshintegration.entity.MeshIntegrationRequest;
import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.salesforce.Client;

import java.util.List;
import java.util.Map;

public interface MeshIntegrationService {
    void process();

    void pushInformationToSalesforce(Map<OrganisationSalesforceInformation, List<Client>> stringListMap);

    List<MeshIntegrationRequest> receiveMeshIntegrationRequestFromQueue();

    Map<OrganisationSalesforceInformation, List<Client>> retrieveClientsFromMeshAPI(List<MeshIntegrationRequest> meshIntegrationRequestList);
}
