package com.ctc.property.meshintegration.service;

import com.ctc.property.meshintegration.Util.MeshIntegrationUtil;
import com.ctc.property.meshintegration.entity.MeshIntegrationRequest;
import com.ctc.property.meshintegration.entity.MeshToken;
import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.salesforce.Client;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@Service
public class MeshIntegrationServiceImpl implements MeshIntegrationService{
    private static final Logger LOG = Logger.getLogger(MeshIntegrationServiceImpl.class.getName());

    @Autowired
    private MessageService messageService;

    @Autowired
    private MeshAPIService meshAPIService;

    @Autowired
    private SalesforceService salesforceService;

    private final static Map<String, MeshToken> meshTokenMap = new HashMap<>();

    @Override
    public void process(){
        LOG.info("MeshIntegrationService is processing");
        pushInformationToSalesforce(retrieveClientsFromMeshAPI(receiveMeshIntegrationRequestFromQueue()));
    }
    @Override
    public void pushInformationToSalesforce(final Map<OrganisationSalesforceInformation, List<Client>> clientOrganisationMap){
        clientOrganisationMap.forEach((organisationSalesforceInformation, clients) -> {
            if (!clients.isEmpty()) {
                LOG.info("Pushing mesh information to salesforce");
                salesforceService.pushClientInformationToOrganisation(organisationSalesforceInformation, clients);
            }
        });
    }

    @Override
    public List<MeshIntegrationRequest> receiveMeshIntegrationRequestFromQueue(){
        LOG.info("Receiving mesh integration request from message queue");
        final List<MeshIntegrationRequest> meshIntegrationRequestList = new ArrayList<>();
        messageService.receiveMessage().forEach(message -> {
            MeshIntegrationUtil.toMeshIntegrationRequest(message.getBody()).map(meshIntegrationRequestList::add);
            messageService.deleteMessage(message);
        });
        LOG.info(meshIntegrationRequestList.size()+" Mesh Integration Request received successfully");
        return meshIntegrationRequestList;
    }

    @Override
    public Map<OrganisationSalesforceInformation, List<Client>> retrieveClientsFromMeshAPI(final List<MeshIntegrationRequest> meshIntegrationRequestList){
        final Map<OrganisationSalesforceInformation, List<Client>> organisationClientsMap = new HashMap<>();
        meshIntegrationRequestList.forEach(meshIntegrationRequest -> {
            LOG.info("Retrieving client information from mesh api");
            meshIntegrationRequest.getProjectDetails().forEach(projectDetail -> {
                LOG.info("Retrieving client information for project "+projectDetail.toString());
                MeshToken meshToken = meshTokenMap.get(projectDetail.getProjectId());
                LOG.info(meshToken!= null?meshToken.toString():"MeshToken is null");
                Optional<MeshToken> optionalMeshToken;
                meshAPIService.initialise(projectDetail.getUrl());
                if (meshToken==null || meshToken.getExpires().isBefore(LocalDateTime.now()) ) {
                    optionalMeshToken = meshAPIService.retrieveToken(projectDetail.getKey(), projectDetail.getPass());
                }
                else {
                    optionalMeshToken = Optional.of(meshToken);
                }

                optionalMeshToken.ifPresent(t->{
                    meshTokenMap.put(projectDetail.getProjectId(),t);
                    meshAPIService.retrieveData(t.getToken(),  MeshIntegrationUtil.toDateTimeStringFormatUsedByMeshAPI(meshIntegrationRequest.getFromDateTime()))
                        .map(MeshIntegrationUtil::retrieveTranscriptIds)
                        .map(transcriptIds -> meshAPIService.retrieveTranscripts(t.getToken(), transcriptIds))
                        .map(MeshIntegrationUtil::toClients)
                        .ifPresent(clients -> {
                            OrganisationSalesforceInformation organisationSalesforceInformation = new OrganisationSalesforceInformation(meshIntegrationRequest.getOrgId(), meshIntegrationRequest.getNamespace());
                            List<Client> existingClients = organisationClientsMap.get(organisationSalesforceInformation);
                            if (existingClients != null){
                                existingClients.addAll(clients);
                            }
                            else {
                                organisationClientsMap.put(organisationSalesforceInformation, clients);
                            }
                        });
                });
            });
        });
        return organisationClientsMap;
    }
}
