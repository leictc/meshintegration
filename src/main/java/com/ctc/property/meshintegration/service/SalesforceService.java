package com.ctc.property.meshintegration.service;

import com.ctc.property.meshintegration.entity.OrganisationSalesforceInformation;
import com.ctc.property.meshintegration.salesforce.Client;

import java.util.List;

public interface SalesforceService {
    void pushClientInformationToOrganisation(OrganisationSalesforceInformation organisationSalesforceInformation, List<Client> clients);
}
