package com.ctc.property.meshintegration.service;

import com.ctc.property.meshintegration.entity.MeshToken;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface MeshAPIService {
    void initialise(String url);

    Optional<MeshToken> retrieveToken(String key, String pass);

    Optional<String> retrieveData(String token, String fromDateTime);

    List<String> retrieveTranscripts(String token, Set<String> transcriptIds);
}
