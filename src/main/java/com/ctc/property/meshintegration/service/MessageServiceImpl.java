package com.ctc.property.meshintegration.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class MessageServiceImpl implements MessageService {
    private static final Logger LOG = Logger.getLogger(MessageServiceImpl.class.getName());
    private AmazonSQS sqs;
    private String messageQueueUrl;


    public MessageServiceImpl(@Value("${sqs.access_key}") final String accessKey, @Value("${sqs.access_token}") final String accessToken,
                              @Value("${sqs.region}") final String region, @Value("${sqs.message_queue_url}") final String messageQueueUrl){
        AWSCredentials awsCredential = new BasicAWSCredentials(checkNotNull(accessKey, "Access Key Is Null"), checkNotNull(accessToken, "Access Token Is Null"));
        AmazonSQSClientBuilder amazonSQSClientBuilder = AmazonSQSClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredential)).withRegion(checkNotNull(region, "Region Is Null"));
        this.messageQueueUrl = messageQueueUrl;
        this.sqs = amazonSQSClientBuilder.build();
    }

    public void sendMessage(String messageBody){
        LOG.info("Sending Message to "+ this.messageQueueUrl);
        SendMessageRequest sendMessageRequest = new SendMessageRequest(this.messageQueueUrl, messageBody);
        sqs.sendMessage(sendMessageRequest);
    }

    public List<Message> receiveMessage(){
        LOG.info("Receiving Message from "+ this.messageQueueUrl);
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(this.messageQueueUrl).withWaitTimeSeconds(20).withMaxNumberOfMessages(10);
        List<Message> messageList = new ArrayList<>();
        int count = 0 ;
        while(count < 50) {
            List<Message> tempMessages = this.sqs.receiveMessage(receiveMessageRequest).getMessages();
            if (tempMessages == null || tempMessages.isEmpty()){
                break;
            }
            messageList.addAll(tempMessages);
            count++;
        }
        return messageList;
    }

    public void deleteMessage(final Message message) {
        LOG.info("Deleting Message "+message+" from "+this.messageQueueUrl);
        DeleteMessageRequest request = new DeleteMessageRequest(this.messageQueueUrl, message.getReceiptHandle());
        this.sqs.deleteMessage(request);
    }

}
